@extends('layouts.app')
@section('content')
    <div class="col-md-12">
        <div class="tab-pane">
            <form action="{{ route('save-template') }}" method="post">
                @csrf
                <div class="tab-pane">
                    <h2>Выберите блоки которые нужны</h2>
                </div>
                @foreach(\App\HtmlBlock::additionalBlock() as $key => $block)
                    <div class="form-group">
                        <label for="{{ $block }}">{{ $key }}</label>
                        <input type="checkbox" name="{{ $block }}" id="{{ $block }}" value="{{ $block }}">
                    </div>
                    @endforeach
                <button type="submit" class="btn btn-success">Сгенерировать</button>
            </form>
        </div>
    </div>
    @endsection
