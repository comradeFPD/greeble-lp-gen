@extends('layouts.app')
@section('content')
    <h3 class="text-center">Для того что бы перегенерировать шаблон обновите страницу</h3>
    <div class="col-md-12">
        <div class="tab-pane text-center">
            <a href="/storage/template/index.html" target="_blank" class="btn btn-info">Посмотреть</a>
            <a href="{{ route('save-archive') }}" class="btn btn-success">Сохранить</a>
        </div>
    </div>
    <script>
        let el = document.getElementById('reset');
        el.onclick = function (e) {
            e.preventDefault();
            document.location.reload();
        }
    </script>
    @endsection
