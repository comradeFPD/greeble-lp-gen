<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HtmlBlock extends Model
{
    /**
     * @var array BLOCK_TYPE Массив с типами блоков
     */
    const BLOCK_TYPE = [
      'Начальный экран' => 'startScreen',
        'Контакты' => 'contacts',
        'Отзывы' => 'reviews',
        'Цены' => 'prices',
        'О нас' => 'about',
        'Футер' => 'footer',
        'Наши работы' => 'ourWorks',
        'О продукте' => 'product',
        'Преимущества' => 'advantages',
        'Призывы' => 'appeals',
        'FAQ' => 'faq',
        'До и после' => 'beforeAfter',
        'С кем работаем' => 'workWith',
        'Сравнение' => 'comparison'
    ];

    public static function getTemplateStart()
    {
        $template = '<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</head>
<body>';
        return $template;
    }

    public static function getEndTemplate($template)
    {
        return $template.= '</body></html>';
    }

    public static function additionalBlock()
    {
        $sorted = [];
        foreach (self::BLOCK_TYPE as $key => $item){
            if($key == 'Сравнение' || $key == 'До и после' || $key == 'Отзывы' || $key == 'FAQ'
                || $key == 'С кем работаем'){
                $sorted[$key] = $item;
            }
        }
        return $sorted;
    }

}
