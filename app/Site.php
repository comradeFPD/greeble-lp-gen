<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Site extends Model
{
    private function getStartScreenBlock()
    {
        return HtmlBlock::whereType(HtmlBlock::BLOCK_TYPE['Начальный экран'])->get()->shuffle()->slice(0,1)
            ->first()->template;
    }

    private function getAdvantagesBlock()
    {
        return HtmlBlock::whereType(HtmlBlock::BLOCK_TYPE['Преимущества'])->get()->shuffle()->slice(0,1)
            ->first()->template;
    }

    private function getContactsBlock()
    {
        return HtmlBlock::whereType(HtmlBlock::BLOCK_TYPE['Контакты'])->get()->shuffle()->slice(0,1)
            ->first()->template;
    }

    private function getReviewsBlock()
    {
        return HtmlBlock::whereType(HtmlBlock::BLOCK_TYPE['Отзывы'])->get()->shuffle()->slice(0,1)
            ->first()->template;
    }

    private function getPricesBlock()
    {
        return HtmlBlock::whereType(HtmlBlock::BLOCK_TYPE['Цены'])->get()->shuffle()->slice(0,1)
            ->first()->template;
    }

    private function getAboutBlock()
    {
        return HtmlBlock::whereType(HtmlBlock::BLOCK_TYPE['О нас'])->get()->shuffle()->slice(0,1)
            ->first()->template;
    }

    private function getFooterBlock()
    {
        return HtmlBlock::whereType(HtmlBlock::BLOCK_TYPE['Футер'])->get()->shuffle()->slice(0,1)
            ->first()->template;
    }

    private function getOurWorksBlock()
    {
        return HtmlBlock::whereType(HtmlBlock::BLOCK_TYPE['Наши работы'])->get()->shuffle()->slice(0,1)
            ->first()->template;
    }

    private function getProductBlock()
    {
        return HtmlBlock::whereType(HtmlBlock::BLOCK_TYPE['О продукте'])->get()->shuffle()->slice(0,1)
            ->first()->template;
    }

    private function getAppealsBlock()
    {
        return HtmlBlock::whereType(HtmlBlock::BLOCK_TYPE['Призывы'])->get()->shuffle()->slice(0,1)
            ->first()->template;
    }

    private function getFaqBlock()
    {
        return HtmlBlock::whereType(HtmlBlock::BLOCK_TYPE['FAQ'])->get()->shuffle()->slice(0,1)
            ->first()->template;
    }

    private function getBeforeAfterBlock()
    {
        return HtmlBlock::whereType(HtmlBlock::BLOCK_TYPE['До и после'])->get()->shuffle()->slice(0,1)
            ->first()->template;
    }

    private function getWorkWithBlock()
    {
        return HtmlBlock::whereType(HtmlBlock::BLOCK_TYPE['С кем работаем'])->get()->shuffle()->slice(0,1)
            ->first()->template;
    }

    private function getComparisonBlock()
    {
        return HtmlBlock::whereType(HtmlBlock::BLOCK_TYPE['Сравнение'])->get()->shuffle()->slice(0,1)
            ->first()->template;
    }

    public function makeTemplate($data)
    {
        $template = HtmlBlock::getTemplateStart();
        $template .= $this->getStartScreenBlock();
        $blocks = collect([$this->getProductBlock(), $this->getAdvantagesBlock(), $this->getAboutBlock(),
            $this->getOurWorksBlock(), $this->getAppealsBlock()]);
        if(isset($data['prices']))
            $blocks->push($this->getPricesBlock());
        if(isset($data['reviews']))
            $blocks->push($this->getReviewsBlock());
        if(isset($data['faq']))
            $blocks->push($this->getFaqBlock());
        if(isset($data['beforeAfter']))
            $blocks->push($this->getBeforeAfterBlock());
        if(isset($data['workWith']))
            $blocks->push($this->getWorkWithBlock());
        if(isset($data['comparison']))
            $blocks->push($this->getComparisonBlock());
        $blocks = $blocks->shuffle();
        foreach ($blocks as $block){
            $template .= $block;
        }
        $template .= $this->getContactsBlock();
        $template .= $this->getFooterBlock();
        return HtmlBlock::getEndTemplate($template);
    }
}
