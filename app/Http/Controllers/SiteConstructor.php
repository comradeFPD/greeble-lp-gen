<?php

namespace App\Http\Controllers;

use App\HtmlBlock;
use App\Site;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class SiteConstructor extends Controller
{
    public function getConstruct()
    {
        return response()->view('constructor');
    }

    public function postSaveTemplate(Request $request)
    {
        $data = $request->except('_token');
        $site = new Site();
        $template = $site->makeTemplate($data);
        Storage::disk('public')->put('/template/index.html', $template);
        return response()->view('successful-generated');

    }

    public function getSaveToDisk()
    {
        $zip = new \ZipArchive();
        $zip->open(public_path('/storage/template.zip'), \ZipArchive::CREATE | \ZipArchive::OVERWRITE);
        $zip->addFile(public_path('/storage/template/index.html'));
        $zip->close();
        return response()->download(public_path('/storage/template.zip'));
    }
}
