<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/constructor', 'SiteConstructor@getConstruct');
Route::post('/save-template', 'SiteConstructor@postSaveTemplate')->name('save-template');
Route::get('/save-archive', 'SiteConstructor@getSaveToDisk')->name('save-archive');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
